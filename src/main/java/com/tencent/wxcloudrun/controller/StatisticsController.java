package com.tencent.wxcloudrun.controller;

import com.alibaba.fastjson.JSONObject;
import com.tencent.wxcloudrun.config.ApiResponse;
import com.tencent.wxcloudrun.dto.StatisticsRequest;
import com.tencent.wxcloudrun.model.StatisticsBasic;
import com.tencent.wxcloudrun.model.StatisticsModel;
import com.tencent.wxcloudrun.service.StatisticsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * 统计的控制器
 */
@RestController
public class StatisticsController {


  final Logger logger;

  final StatisticsService statisticsService;

  public StatisticsController(StatisticsService statisticsService) {
    this.statisticsService = statisticsService;
    this.logger = LoggerFactory.getLogger(StatisticsController.class);
  }

  /**
   * 统计基础信息
   */
  @PostMapping(value = "/statistics/basic")
  ApiResponse getStatisticsBasic(@RequestBody StatisticsRequest request) {
    logger.info("Request 统计基础信息, 参数：" + JSONObject.toJSONString(request));

    try {

      StatisticsBasic statisticsBasic = statisticsService.getStatisticsBasic(request);

      Map<String, Object> data = new HashMap<>();
      data.put("data", statisticsBasic);

      return ApiResponse.ok(data);
    } catch (Exception e) {
      logger.error("controller 异常 统计基础信息异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 统计订单数据
   */
  @PostMapping(value = "/statistics/orderStatistics")
  ApiResponse getOrderStatistics(@RequestBody StatisticsRequest request) {
    logger.info("Request 统计订单数据, 参数：" + JSONObject.toJSONString(request));

    try {

      List<StatisticsModel> statisticsModelList = statisticsService.getOrderStatistics(request);
      Map<String, Object> data = new HashMap<>();
      data.put("list", statisticsModelList);

      return ApiResponse.ok(data);
    } catch (Exception e) {
      logger.error("controller 异常 统计订单数据异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 统计用户活跃度数据
   */
  @PostMapping(value = "/statistics/userStatistics")
  ApiResponse getUserStatistics(@RequestBody StatisticsRequest request) {
    logger.info("Request 统计用户活跃度数据, 参数：" + JSONObject.toJSONString(request));

    try {

      List<StatisticsModel> statisticsModelList = statisticsService.getUserStatistics(request);
      Map<String, Object> data = new HashMap<>();
      data.put("list", statisticsModelList);

      return ApiResponse.ok(data);
    } catch (Exception e) {
      logger.error("controller 异常 统计用户活跃度数据异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 各商品品类销量统计
   */
  @PostMapping(value = "/statistics/goodsClassStatistics")
  ApiResponse getGoodsClassSaleStatistics(@RequestBody StatisticsRequest request) {
    logger.info("Request 各商品品类销量统计, 参数：" + JSONObject.toJSONString(request));

    try {

      List<StatisticsModel> statisticsModelList = statisticsService.getGoodsClassSaleStatistics(request);
      Map<String, Object> data = new HashMap<>();
      data.put("list", statisticsModelList);

      return ApiResponse.ok(data);
    } catch (Exception e) {
      logger.error("controller 异常 各商品品类销量统计异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 单品销售统计
   */
  @PostMapping(value = "/statistics/goodsSaleStatistics")
  ApiResponse getGoodsSaleStatistics(@RequestBody StatisticsRequest request) {
    logger.info("Request 单品销售统计, 参数：" + JSONObject.toJSONString(request));

    try {

      List<StatisticsModel> statisticsModelList = statisticsService.getGoodsSaleStatistics(request);
      Map<String, Object> data = new HashMap<>();
      data.put("list", statisticsModelList);

      return ApiResponse.ok(data);
    } catch (Exception e) {
      logger.error("controller 异常 单品销售统计异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

}