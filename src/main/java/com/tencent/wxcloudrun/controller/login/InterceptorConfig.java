package com.tencent.wxcloudrun.controller.login;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.handler.MappedInterceptor;

@Configuration
public class InterceptorConfig {
    @Bean
    public MappedInterceptor loginInterceptor(){
        // 设置拦截器的作用域
        //登录页面、图片访问、小程序访问
        return new MappedInterceptor(new String[]{"/**"},new String[]{"/login", "/app/*", "/app/*/*", "/file/*", "/file/*/*", "/file/*/*/*"},new LoginInterceptor());
    }
}
