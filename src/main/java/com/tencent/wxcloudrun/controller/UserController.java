package com.tencent.wxcloudrun.controller;

import com.alibaba.fastjson.JSONObject;
import com.tencent.wxcloudrun.config.ApiResponse;
import com.tencent.wxcloudrun.dto.*;
import com.tencent.wxcloudrun.model.*;
import com.tencent.wxcloudrun.service.ReceiverService;
import com.tencent.wxcloudrun.service.UserService;
import com.tencent.wxcloudrun.util.ConstUtils;
import com.tencent.wxcloudrun.util.TokenUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;

/**
 * counter控制器
 */
@RestController
public class UserController {

  final UserService userService;

  final ReceiverService receiverService;
  final Logger logger;

  //设置免费的会员天数
  final static Integer FREE_VIP_DAYS = 30;

  public UserController(UserService userService, ReceiverService receiverService) {
    this.userService = userService;
    this.receiverService = receiverService;
    this.logger = LoggerFactory.getLogger(UserController.class);
  }


  /**
   * 根据手机号获取用户中心数据
   * @param request {@link CounterRequest}
   * @return API response json
   */
  @PostMapping(value = "/app/getUserCenter")
  ApiResponse getUser(@RequestBody UserRequest request) {
    logger.info("Request 根据手机号获取用户信息，参数" +  JSONObject.toJSONString(request));

    try {
      User user = userService.getUserCenterByTel(request.getTel());
      return ApiResponse.ok(user);
    } catch (Exception e) {
      logger.error("controller 异常 根据手机号获取用户信息异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 根据手机号获取用户基础信息
   * @param request {@link CounterRequest}
   * @return API response json
   */
  @PostMapping(value = "/app/getUserBasicInfo")
  ApiResponse getUserBasicInfo(@RequestBody UserRequest request) {
    logger.info("Request 根据手机号获取用户信息，参数" +  JSONObject.toJSONString(request));

    try {
      User user = userService.getUserBasicInfoByTel(request.getTel());
      return ApiResponse.ok(user);
    } catch (Exception e) {
      logger.error("controller 异常 根据手机号获取用户基础信息异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }


  /**
   * 插入用户信息
   * @param request {@link CounterRequest}
   * @return API response json
   */
  @PostMapping(value = "/app/insertUser")
  ApiResponse insertUser(@RequestBody UserRequest request) {
    logger.info("Request 插入用户信息, 参数：" + JSONObject.toJSONString(request));

    try {

      //todo 还有验证码需要去验证

      //如果还是没有该用户 则插入一个
      Long nowTime = (new Date()).getTime();
      User user = new User();
      user.setTel(request.getTel());
      user.setAvatar(request.getAvatar());
      user.setNickname(request.getNickname());
      user.setVipStartTime(nowTime);
      user.setVipEndTime(nowTime + FREE_VIP_DAYS * 24 * 60 * 60 * 1000L);  //新增的用户默认一个月的免费vip体验时间
      user.setCreateTime(nowTime);
      user.setUpdateTime(nowTime);
      user.setAccountType(ConstUtils.ACCOUNT_TYPE_CUSTOMER); //用户类型
      userService.insertUser(user);

      return ApiResponse.ok(user);
    } catch (Exception e) {
      logger.error("controller 异常 插入用户信息异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 更新用户vip时间信息
   * @param request {@link CounterRequest}
   * @return API response json
   */
  @PostMapping(value = "/api/updateUserVipTime")
  ApiResponse updateUserVipTime(@RequestBody UserRequest request) {
    logger.info("Request 更新用户信息, 参数：" + JSONObject.toJSONString(request));

    try {
      Long nowTime = (new Date()).getTime();
      User user = new User();
      user.setTel(request.getTel());
      user.setVipStartTime(request.getVipStartTime());
      user.setVipEndTime(request.getVipEndTime());
      user.setUpdateTime(nowTime);

      userService.updateUserVipTime(user);

      return ApiResponse.ok("success");
    } catch (Exception e) {
      logger.error("controller 异常 更新用户信息异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 根据手机号获取用户地址信息
   * @param request {@link CounterRequest}
   * @return API response json
   */
  @PostMapping(value = "/app/user/getReceiver")
  ApiResponse getReceiver(@RequestBody ReceiverRequest request) {
    logger.info("Request 根据手机号获取地址信息，参数" +  JSONObject.toJSONString(request));

    try {
      List<Receiver> receiver = receiverService.getReceiverByTel(request.getTel());
      return ApiResponse.ok(receiver);
    } catch (Exception e) {
      logger.error("controller 异常 根据手机号获取用户地址信息异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 插入用户地址信息
   * @param request {@link CounterRequest}
   * @return API response json
   */
  @PostMapping(value = "/app/insertReceiver")
  ApiResponse insertReceiver(@RequestBody ReceiverRequest request) {
    logger.info("Request 插入用户地址信息, 参数：" + JSONObject.toJSONString(request));

    try {
      Long nowTime = (new Date()).getTime();
      Receiver receiver = new Receiver();
      receiver.setId(UUID.randomUUID().toString());
      receiver.setTel(request.getTel());
      receiver.setReceiverName(request.getReceiverName());
      receiver.setReceiverTel(request.getReceiverTel());
      receiver.setProvince(request.getProvince());
      receiver.setCity(request.getCity());
      receiver.setDistrict(request.getDistrict());
      receiver.setAddrDetail(request.getAddrDetail());
      receiver.setEstate(request.getEstate());
      receiver.setCreateTime(nowTime);
      receiver.setUpdateTime(nowTime);
      receiverService.insertReceiver(receiver);

      return ApiResponse.ok("success");
    } catch (Exception e) {
      logger.error("controller 异常 插入用户地址信息异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 更新用户地址信息
   * @param request {@link CounterRequest}
   * @return API response json
   */
  @PostMapping(value = "/app/updateReceiver")
  ApiResponse updateReceiver(@RequestBody ReceiverRequest request) {
    logger.info("Request 更新用户地址信息, 参数：" + JSONObject.toJSONString(request));

    try {
      Long nowTime = (new Date()).getTime();
      Receiver receiver = new Receiver();
      receiver.setTel(request.getTel());
      receiver.setReceiverName(request.getReceiverName());
      receiver.setReceiverTel(request.getReceiverTel());
      receiver.setProvince(request.getProvince());
      receiver.setCity(request.getCity());
      receiver.setDistrict(request.getDistrict());
      receiver.setAddrDetail(request.getAddrDetail());
      receiver.setUpdateTime(nowTime);

      receiverService.updateReceiver(receiver);

      return ApiResponse.ok("success");
    } catch (Exception e) {
      logger.error("controller 异常 更新用户地址信息异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 员工登录页面
   */
  @PostMapping(value = "/login")
  ApiResponse login(@RequestBody UserRequest request) {
    logger.info("Request 员工登录页面, 参数：" + JSONObject.toJSONString(request));

    try {

      User user = userService.getUserByTelBasic(request.getTel());

      if(user == null) {
        return new ApiResponse(-1, "登录用户不存在！", null);
      }
      if (!user.getPassword().equals(request.getPassword())) {
        return new ApiResponse(-1, "账户密码错误！", null);
      }

//      String tokenStr = TokenUtils.getToken(request.getTel(), request.getPassword());
      String tokenStr = TokenUtils.sign(user.getTel());

      logger.info("username: " + request.getTel() + " token: " + tokenStr);
      Map<String, String> tokenMap = new HashMap<>();
      tokenMap.put("token", tokenStr);
//      tokenMap.put("tokenHead", TokenUtils.TOKEN_HEAD);

      tokenMap.put("tokenHead", "");




      return ApiResponse.ok(tokenMap);
    } catch (Exception e) {
      logger.error("controller 异常 员工登录页面异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 获取人员的基本信息
   */
  @PostMapping(value = "/api/info")
  ApiResponse userInfo(@RequestBody UserRequest request) {
    logger.info("Request 员工登录页面, 参数：" + JSONObject.toJSONString(request));

    try {
      Map<String, Object> data = new HashMap<>();
      //这里的username实际就是用户的手机号
      User user = userService.getUserByTelBasic(request.getTel());
      if(user == null) {
        return new ApiResponse(-1, "用户不存在！", null);
      }
      data.put("username", request.getTel());
      data.put("icon", user.getAvatar());
      data.put("area", user.getArea());
      data.put("distributionStation", user.getDistributionStation());

      List<Role> roleList = userService.getRoleByTel(request.getTel());
      if( roleList != null && !roleList.isEmpty()){
        List<String> roles = roleList.stream().map(Role::getRoleName).collect(Collectors.toList());
        data.put("roles",roles);
      }
      List<Menus> menusList = userService.getMenusByTel(request.getTel());
      data.put("menus", menusList);

      return ApiResponse.ok(data);
    } catch (Exception e) {
      logger.error("controller 异常 获取用户信息异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 根据条件查询用户列表
   */
  @PostMapping(value = "/api/userList")
  ApiResponse userList(@RequestBody UserRequest request) {
    logger.info("Request 分页获取用户列表, 参数：" + JSONObject.toJSONString(request));

    try {
      //这里的username实际就是用户的手机号
      Map<String, Object> paramMap = new HashMap<>();
      paramMap.put("keyWord", request.getKeyWord());
      paramMap.put("area", request.getArea());
      paramMap.put("distributionStation", request.getDistributionStation());
      paramMap.put("currentPage", (request.getCurrentPage()-1) * request.getPageSize()); //由于前端传去的参数为1开始 所以这里需要减一
      paramMap.put("pageSize", request.getPageSize());

      List<User> userList = userService.getUserByConditions(paramMap);
      Map<String, Object> data = new HashMap<>();
      data.put("list", userList);
      Integer totalNum = userService.getUserTotalNumByConditions(paramMap);
      data.put("total", totalNum != null ? totalNum : 0);

      return ApiResponse.ok(data);
    } catch (Exception e) {
      logger.error("controller 异常 根据条件查询用户列表信息异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 根据条件查询角色列表
   */
  @PostMapping(value = "/role/list")
  ApiResponse userList(@RequestBody RoleRequest request) {
    logger.info("Request 获取角色列表, 参数：" + JSONObject.toJSONString(request));

    try {

      //这里的username实际就是用户的手机号
      List<Role> roleList = userService.getRolesByConditions(request.getRoleName(), request.getCurrentPage(), request.getPageSize());
      Map<String, Object> data = new HashMap<>();
      data.put("list", roleList);
      data.put("total", roleList != null ? roleList.size() : 0);

      return ApiResponse.ok(data);
    } catch (Exception e) {
      logger.error("controller 异常 根据条件查询角色信息异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 更新用户的权限
   */
  @PostMapping(value = "/role/update")
  ApiResponse updateRole(@RequestBody UserRequest request) {
    logger.info("Request 更新用户权限, 参数：" + JSONObject.toJSONString(request));

    try {
      userService.updateRole(request.getTel(), request.getRoleIds());
      return ApiResponse.ok("success");
    } catch (Exception e) {
      logger.error("controller 异常 更新用户的权限信息异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 更新用户的状态
   */
  @PostMapping(value = "/user/updateStatus")
  ApiResponse updateUserStatus(@RequestBody UserRequest request) {
    logger.info("Request 更新用户状态, 参数：" + JSONObject.toJSONString(request));

    try {
      userService.updateUserStatus(request.getTel(), request.getActived());
      return ApiResponse.ok("success");
    } catch (Exception e) {
      logger.error("controller 异常 更新用户状态异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 获取配送站列表
   */
  @PostMapping(value = "/user/areaList")
  ApiResponse getAreaList(@RequestBody UserRequest request) {
    logger.info("Request 获取区域列表, 参数：" + JSONObject.toJSONString(request));
    try {

      Map<String, Object> paramMap = new HashMap<>();
      paramMap.put("areaType", "2");

      List<CommonArea> areaList = userService.getCommonAreaByConditions(paramMap);
      Map<String, Object> data = new HashMap<>();
      data.put("areaList", areaList);

      return ApiResponse.ok(data);
    } catch (Exception e) {
      logger.error("controller 异常 获取区域列表异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 获取配送站列表
   */
  @PostMapping(value = "/user/distributionStationList")
  ApiResponse getDistributionStationList(@RequestBody CommonAreaRequest request) {
    logger.info("Request 获取配送站列表, 参数：" + JSONObject.toJSONString(request));

    try {
      Map<String, Object> paramMap = new HashMap<>();
      paramMap.put("parentId", request.getParentId());
      paramMap.put("areaType", "3");

      List<CommonArea> dsList = userService.getCommonAreaByConditions(paramMap);
      Map<String, Object> data = new HashMap<>();
      data.put("dsList", dsList);

      return ApiResponse.ok(data);
    } catch (Exception e) {
      logger.error("controller 异常 获取区域 配送站列表异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 获取小区列表
   */
  @PostMapping(value = "/user/estateList")
  ApiResponse getEstateList(@RequestBody CommonAreaRequest request) {
    logger.info("Request 获取小区列表, 参数：" + JSONObject.toJSONString(request));

    try {
      Map<String, Object> paramMap = new HashMap<>();
      paramMap.put("parentId", request.getParentId());
      paramMap.put("areaType", "4");  //小区的类型

      List<CommonArea> dsList = userService.getCommonAreaByConditions(paramMap);
      Map<String, Object> data = new HashMap<>();
      data.put("estateList", dsList);

      return ApiResponse.ok(data);
    } catch (Exception e) {
      logger.error("controller 异常 获取小区列表异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 创建员工账号
   */
  @PostMapping(value = "/user/create")
  ApiResponse createStaffUser(@RequestBody UserRequest request) {
    logger.info("Request 创建员工账号, 参数：" + JSONObject.toJSONString(request));

    try {
        User user = new User();
        user.setNickname(request.getNickname());
        user.setTel(request.getTel());
        user.setPassword(request.getPassword());
        user.setArea(request.getArea());
        user.setDistributionStation(request.getDistributionStation());
        user.setRemark(request.getRemark());
        user.setActived(request.getActived());
        user.setCreateTime((new Date()).getTime());
        user.setUpdateTime((new Date()).getTime());
        user.setAccountType("1"); // 类型 1 表示员工账号

        userService.insertStaffUser(user);
      return ApiResponse.ok("success");
    } catch (Exception e) {
      logger.error("controller 异常 创建员工账号，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 更新账号
   */
  @PostMapping(value = "/user/update")
  ApiResponse updateUser(@RequestBody UserRequest request) {
    logger.info("Request 更新用户账号, 参数：" + JSONObject.toJSONString(request));

    try {
      User user = new User();
      user.setNickname(request.getNickname());
      user.setTel(request.getTel());
      user.setPassword(request.getPassword());
      user.setArea(request.getArea());
      user.setDistributionStation(request.getDistributionStation());
      user.setRemark(request.getRemark());
      user.setActived(request.getActived());
      user.setUpdateTime((new Date()).getTime());

      userService.updateUserBasicInfo(user);
      return ApiResponse.ok("success");
    } catch (Exception e) {
      logger.error("controller 异常 更新账号信息异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 删除用户
   */
  @PostMapping(value = "/user/delete")
  ApiResponse deleteUser(@RequestBody UserRequest request) {
    logger.info("Request 删除用户信息, 参数：" + JSONObject.toJSONString(request));

    try {
      userService.deleteUser(request.getTel());
      return ApiResponse.ok("success");
    } catch (Exception e) {
      logger.error("controller 异常 删除用户信息，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }

  /**
   * 通过区域获取小区列表
   */
  @PostMapping(value = "/app/user/getEstateListByArea")
  ApiResponse getEstateListByArea(@RequestBody CommonAreaRequest request) {
    logger.info("Request 通过区域获取小区列表, 参数：" + JSONObject.toJSONString(request));

    try {
      Map<String, Object> paramMap = new HashMap<>();
      paramMap.put("areaId", request.getId());

      List<CommonArea> estateList = userService.getEstateListByArea(paramMap);
      Map<String, Object> data = new HashMap<>();
      data.put("list", estateList);

      return ApiResponse.ok(data);
    } catch (Exception e) {
      logger.error("controller 异常 通过区域获取小区列表，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }



}