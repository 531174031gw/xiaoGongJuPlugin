package com.tencent.wxcloudrun.controller;

import com.alibaba.fastjson.JSONObject;
import com.tencent.wxcloudrun.config.ApiResponse;
import com.tencent.wxcloudrun.dto.CommonToolsRequest;
import com.tencent.wxcloudrun.dto.StatisticsRequest;
import com.tencent.wxcloudrun.model.Dictionary;
import com.tencent.wxcloudrun.model.StatisticsBasic;
import com.tencent.wxcloudrun.model.StatisticsModel;
import com.tencent.wxcloudrun.service.CommonToolsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 统计的控制器
 */
@RestController
public class CommonToolsController {


  final Logger logger;

  final CommonToolsService commonToolsService;

  public CommonToolsController(CommonToolsService commonToolsService) {
    this.commonToolsService = commonToolsService;
    this.logger = LoggerFactory.getLogger(CommonToolsController.class);
  }

  /**
   * 根据字典类型获取字典
   */
  @PostMapping(value = "/commonTools/dictionary/list")
  ApiResponse getDictionaryList(@RequestBody CommonToolsRequest request) {
    logger.info("Request 根据字典类型获取字典, 参数：" + JSONObject.toJSONString(request));

    try {
      if(request.getDictTypeList() == null || request.getDictTypeList().isEmpty()) {
        return ApiResponse.error("获取字典信息失败！");
      }
      Map<String, Object> data = new HashMap<>();
      for(String dictType: request.getDictTypeList()) {
        List<Dictionary> dictionaryList = commonToolsService.getDictionaryListByType(dictType);
        data.put(dictType, dictionaryList);
      }

      return ApiResponse.ok(data);
    } catch (Exception e) {
      logger.error("controller 异常 根据字典类型获取字典异常，" + e.getMessage());
      return ApiResponse.error("服务器内部错误！");
    }
  }


}