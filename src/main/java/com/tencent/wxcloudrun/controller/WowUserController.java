package com.tencent.wxcloudrun.controller;

import com.alibaba.fastjson.JSONObject;
import com.tencent.wxcloudrun.config.ApiResponse;
import com.tencent.wxcloudrun.dto.WowUserRequest;
import com.tencent.wxcloudrun.model.WowUser;
import com.tencent.wxcloudrun.model.WowUserStatus;
import com.tencent.wxcloudrun.service.WowUserService;
import com.tencent.wxcloudrun.service.impl.FileService;
import com.tencent.wxcloudrun.service.impl.WxHelpService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * counter控制器
 */
@RestController
public class WowUserController {

  final WowUserService wowUserService;
  final Logger logger;
  final WxHelpService wxHelpService;
  @Autowired
  FileService fileService;

  final static String SERVERNAME = "https://yingyanzhineng-1818439-1311138730.ap-shanghai.run.tcloudbase.com";

  public WowUserController(@Autowired WowUserService wowUserService, WxHelpService wxHelpService) {
    this.wowUserService = wowUserService;
    this.wxHelpService = wxHelpService;
    this.logger = LoggerFactory.getLogger(WowUserController.class);
  }


  /**
   * 获取用户信息
   * @return API response json
   */
  @PostMapping(value = "/api/getWowUser")
  ApiResponse getUser(@RequestBody WowUserRequest request) {
    logger.info("/api/getUser get request");
    //打印请求参数
    logger.info("请求参数 get user: " + JSONObject.toJSONString(request));
    WowUser wowUser = wowUserService.getUser(request.getTel());
    if(wowUser != null) {
      //判断是否为vip
      Date date = new Date();
      long now = date.getTime();
      if (now <= Long.parseLong(wowUser.getVipOverdue())) { //如果当前时间小于过期时间 则为会员
        wowUser.setIsVip("1");
        logger.info("是会员");
      } else {
        wowUser.setIsVip("0");
      }
      wowUser.setReqStatus("success");
      logger.info("返回参数 get user: " + JSONObject.toJSONString(wowUser));
    } else {
      wowUser = new WowUser();
      wowUser.setReqStatus("fail");
    }
    return ApiResponse.ok(wowUser);
  }

  /**
   * 插入用户
   * @return API response json
   */
  @PostMapping(value = "/api/insertWowUser")
  ApiResponse insertUser(@RequestBody WowUserRequest request) {
    //打印请求参数
    logger.info("请求参数 inser user: " + JSONObject.toJSONString(request));

    //先查询一下是否已存在了
    WowUser wowUser = null;
    wowUser = wowUserService.getUser(request.getTel());
    if(wowUser != null && wowUser.getTel() != null) {
      return  ApiResponse.ok(wowUser);
    }

    logger.info("/api/insertUser get request");
    wowUser = new WowUser();
    wowUser.setTel(request.getTel());
    wowUser.setAvator(request.getAvator());
    wowUser.setNickName(request.getNickName());
    //第一次的用户 都赠送7天的使用
    Date date = new Date();
    long vipOverdueTime = date.getTime() + 15 * 24 * 60 * 60 * 1000;  //当前时间往后推7天  先测试15天的试用期
    wowUser.setVipOverdue( String.valueOf(vipOverdueTime ));
    wowUser.setCreateTime( String.valueOf(date.getTime()));
    wowUser.setUpdateTime( String.valueOf(date.getTime()));

    try {
      //获取用户的openid
      wowUser.setOpenId(wxHelpService.getUserOpenId(request.getLoginCode()));
      wowUserService.insertUser(wowUser);
      //保存成功 则把信息返回给签订
      return  ApiResponse.ok(wowUser);
    }catch (Exception e) {
      logger.error(e.getMessage());
      wowUser.setTel(null);  //保存失败则把手机号置空 返回给前端
      return  ApiResponse.ok(wowUser);
    }
  }

  /**
   * 更新用户支付状态
   * @return API response json
   */
  @PostMapping(value = "/api/updateUserCharge")
  ApiResponse updateUserCharge(@RequestBody WowUserRequest request) {
    //打印请求参数
    logger.info("请求参数 update user charge: " + JSONObject.toJSONString(request));
    //先查询一下是否已存在了
    WowUser wowUser = null;
    wowUser = wowUserService.getUser(request.getTel());
    if(wowUser == null || wowUser.getTel() == null) {
      return  ApiResponse.ok(wowUser);
    }

    logger.info("/api/insertUser get request");
    Date date = new Date();
    wowUser.setChargeStatus(request.getChargeStatus());
    wowUser.setUpdateTime( String.valueOf(date.getTime()));

    try {
      wowUserService.updateUser(wowUser);
      //保存成功 则把信息返回给签订,并且给管理员推送消息
      WowUser admin = wowUserService.getUser("18580203199");
      wxHelpService.sendMsgToUser(admin, wowUser.getUpdateTime(), "有人付款了", wowUser.getNickName() + " " + wowUser.getTel());

      return  ApiResponse.ok(wowUser);
    }catch (Exception e) {
      logger.error(e.getMessage());
      wowUser.setTel(null);  //保存失败则把手机号置空 返回给前端
      return  ApiResponse.ok(wowUser);
    }
  }

  /**
   * 给用户授权
   * @return API response json
   */
  @PostMapping(value = "/api/updateUserOverdue")
  ApiResponse updateUserOverdue(@RequestBody WowUserRequest request) {
    //打印请求参数
    logger.info("请求参数 update user: " + JSONObject.toJSONString(request));
    //先查询一下是否已存在了
    WowUser wowUser = wowUserService.getUser(request.getTel());
    if(wowUser == null || wowUser.getTel() == null) {
      return  ApiResponse.ok(wowUser);
    }

    logger.info("/api/insertUser get request");
    //第一次的用户 都赠送7天的使用
    Date date = new Date();
    long vipOverdueTime = date.getTime() + 365L * 24 * 60 * 60 * 1000;  //当前时间往后推365天  即一次性买断一年的
    wowUser.setVipOverdue(String.valueOf(vipOverdueTime));
    wowUser.setChargeStatus(request.getChargeStatus());
    wowUser.setUpdateTime( String.valueOf(date.getTime()));

    try {
      wowUserService.updateUser(wowUser);
      //保存成功后给授权的用户推送消息
      wxHelpService.sendMsgToUser(wowUser, wowUser.getUpdateTime(), "权限变更", "您的权限已开通！");

      //把过期时间转换成 格式时间
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
      wowUser.setVipOverdue(sdf.format(new Date(Long.parseLong(wowUser.getVipOverdue()))));
      return  ApiResponse.ok(wowUser);
    }catch (Exception e) {
      logger.error(e.getMessage());
      wowUser.setTel(null);  //保存失败则把手机号置空 返回给前端
      return  ApiResponse.ok(wowUser);
    }
  }

  /**
   * 获取用户状态信息
   * @return API response json
   */
  @PostMapping(value = "/api/getUserStatus")
  ApiResponse getUserStatus(@RequestBody WowUserRequest request) {
    //打印请求参数
    logger.info("请求参数 get user status: " + JSONObject.toJSONString(request));
    //先查询一下是否已存在了
    WowUser wowUser = null;
    wowUser = wowUserService.getUser(request.getTel());
    if (wowUser == null) {
      return null;
    }
    WowUserStatus wowUserStatus = wowUserService.getUserStatus(request.getTel());
    //如果用户还没用使用pc客户端，则返回给用户
    if (wowUserStatus == null) {
      wowUserStatus = new WowUserStatus();
      wowUserStatus.setIsVip("-1"); //表示用户还没安装pc客户端
    } else {
      //判断是否为vip
      Date date = new Date();
      long now = date.getTime();
      if (now <= Long.parseLong(wowUser.getVipOverdue())) { //如果当前时间小于过期时间 则为会员
        wowUserStatus.setIsVip("1");
        logger.info("是会员");
      } else {
        logger.info("不是会员");
        wowUserStatus.setIsVip("0");
      }
      logger.info("当前时间 " + now + "  会员到期时间 " + wowUser.getVipOverdue());
      //图片更新时间转换
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      String sd = sdf.format(new Date(Long.parseLong(wowUserStatus.getSnapTime())));        // 时间戳转换成时间
      wowUserStatus.setSnapTime(sd);
    }
    wowUserStatus.setMsgCount(wowUser.getMsgCount()); //赋值用户剩余的通知次数
    wowUserStatus.setIsAcceptMsg(wowUser.getIsAcceptMsg()); //用户是否愿意接受消息
    wowUserStatus.setChargeStatus(wowUser.getChargeStatus()); //赋值用户当前的支付状态
    wowUserStatus.setUserCount(String.valueOf(wowUserService.countUser()));
    logger.info("返回参数 get user status: " + JSONObject.toJSONString(wowUserStatus));
    List<WowUser> users = wowUserService.getUserTop10();
    List<String> avatorList = new ArrayList<>();
    if (users != null) {
      for (WowUser u: users) {
        avatorList.add(u.getAvator());
      }
    }
    wowUserStatus.setAvatorList(avatorList);
    return ApiResponse.ok(wowUserStatus);
  }

  /**
   * 插入用户状态数据
   * @return API response json
   */
  @PostMapping(value = "/api/updateUserStatus")
  ApiResponse putUserStatus(@RequestBody WowUserRequest request) throws ParseException {
    //打印请求参数
    logger.info("请求参数 update user status: " + JSONObject.toJSONString(request));
    //先查询一下是否已存在了
    WowUserStatus wowUserStatus = null;
    Date date = new Date();
    //snaptime的格式转换
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date date1 = simpleDateFormat.parse(request.getSnapTime());
    //图片地址
    String snapPicture = SERVERNAME + "/pic/file/" + request.getSnapPicture();
    //上一次的用户在线状态
    String lastIsOnline = null;
    try {
      wowUserStatus = wowUserService.getUserStatus(request.getTel());
      if (wowUserStatus == null) {
        wowUserStatus = new WowUserStatus();
        wowUserStatus.setTel(request.getTel());
        wowUserStatus.setIsOnline(request.getIsOnline());
        wowUserStatus.setSnapPicture(snapPicture);
        //从pc客户端过来的时间数据需要转换
        wowUserStatus.setSnapTime(String.valueOf(date1.getTime()));
        wowUserStatus.setUpdateTime(String.valueOf(date.getTime()));
        wowUserService.insertUserStatus(wowUserStatus);
        return ApiResponse.ok(wowUserStatus);
      }
      lastIsOnline = wowUserStatus.getIsOnline();
      wowUserStatus.setSnapTime(String.valueOf(date1.getTime()));
      wowUserStatus.setIsOnline(request.getIsOnline());
      wowUserStatus.setSnapPicture(snapPicture);
      wowUserStatus.setUpdateTime(String.valueOf(date.getTime()));
      wowUserService.updateUserStatus(wowUserStatus);
      wowUserStatus.setReqStatus("success");

      //在更新完数据库之后需要如果是掉线了，需要给用户发送消息
      WowUser wowUser = null;
      wowUser = wowUserService.getUser(request.getTel());
      if (wowUser == null) {
        logger.info("当前用户信息不存在，请检查！ tel " + request.getTel());
        return null;
      }
      //只有在用户掉线的情况下才需要去推送消息
      if("0".equals(wowUserStatus.getIsOnline())) {
        wxHelpService.sendMsgToUser(wowUser, wowUserStatus.getSnapTime(), "状态有变", "角色掉线了，鹰眼尝试给你重新链接，请关注");
      } else if ("1".equals(request.getIsOnline()) && "0".equals(lastIsOnline)) {  //如果上一次状态是离线 这次是在线，则发送通知给用户
        wxHelpService.sendMsgToUser(wowUser, wowUserStatus.getSnapTime(), "状态有变", "角色已重新上线，请打开小程序查看最新状态");
      }

    } catch (Exception e) {
      logger.error("更新用户状态失败： " + e.getMessage());
      if (wowUserStatus == null) {
        wowUserStatus = new WowUserStatus();
      }
      wowUserStatus.setReqStatus("fail");
    }

    return ApiResponse.ok(wowUserStatus);
  }


  /**
   *
   * @return API response json
   */
  @PostMapping(value = "/api/addMsgCount")
  ApiResponse addMsgCount(@RequestBody WowUserRequest request) {
    //打印请求参数
    logger.info("请求参数 add msg count: " + JSONObject.toJSONString(request));
    //先查询一下是否已存在了
    WowUser wowUser = null;
    wowUser = wowUserService.getUser(request.getTel());
    if(wowUser == null || wowUser.getTel() == null) {
      return  ApiResponse.ok(wowUser);
    }

    //消息通知次数加一
    wowUser.setMsgCount( String.valueOf( Integer.parseInt(wowUser.getMsgCount()) + 1 ));
    try {
      wowUserService.updateUserMsgCount(wowUser);
      //保存成功 则把信息返回给签订
      logger.info("返回参数 add msg count: " + JSONObject.toJSONString(wowUser));
      return  ApiResponse.ok(wowUser);
    }catch (Exception e) {
      logger.error(e.getMessage());
      wowUser.setTel(null);  //保存失败则把手机号置空 返回给前端
      logger.info("返回参数 add msg count: " + JSONObject.toJSONString(wowUser));
      return  ApiResponse.ok(wowUser);
    }
  }

  /*
    * 图片文件上传
   */
  @PostMapping("/api/upload")
  public String UploadPicture(@RequestParam("file") MultipartFile file) throws IOException {

      logger.info("save file name {}", file.getOriginalFilename());
      return fileService.saveFile(file, null);
  }

  /**
   * 获取待授权的用户列表
   * @return API response json
   */
  @PostMapping(value = "/api/getForAuthUserList")
  ApiResponse getForAuthUserList(@RequestBody WowUserRequest request) {
    logger.info("/api/getForAuthUserList get request");
    List<WowUser> wowUsers = wowUserService.getForAuthUsers();
    if(wowUsers == null || wowUsers.size() <= 0) {
      return null;
    };
    List<WowUser> newUsers = new ArrayList<>();
    //转换时间
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    for (WowUser wu: wowUsers) {
      WowUser item = new WowUser();
      item.setTel(wu.getTel());
      item.setNickName(wu.getNickName());
      item.setAvator(wu.getAvator());
      item.setVipOverdue(sdf.format(new Date(Long.parseLong(wu.getVipOverdue()))));
      item.setUpdateTime(sdf.format(new Date(Long.parseLong(wu.getUpdateTime()))));
      item.setCreateTime(sdf.format(new Date(Long.parseLong(wu.getCreateTime()))));
      item.setChargeStatus(wu.getChargeStatus());
      newUsers.add(item);
    }

    logger.info("/api/getForAuthUserList get request" + newUsers);
    return ApiResponse.ok(newUsers);
  }


}