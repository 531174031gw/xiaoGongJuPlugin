package com.tencent.wxcloudrun.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class Movie implements Serializable {

  private String id;

  private String title;

  private String desc;

  private String type;

  private String subType;

  private String isVipOnly;

  private String url;

  private Long createTime;

  private Long updateTime;
}
