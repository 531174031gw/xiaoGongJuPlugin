package com.tencent.wxcloudrun.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class Dictionary implements Serializable {

  private Integer id;

  private String type;

  private String key; //操作人

  private String value;

}
