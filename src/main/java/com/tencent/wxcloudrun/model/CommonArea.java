package com.tencent.wxcloudrun.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class CommonArea implements Serializable {

  private String id;

  private String name;

  private String parentId;

  private String areaType; // 区域类型 1：area  2： 配送站  3：小区

}
