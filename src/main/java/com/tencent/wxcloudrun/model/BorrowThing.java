package com.tencent.wxcloudrun.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class BorrowThing implements Serializable {

  private String id;

  private String title;

  private String desc;

  private String type;

  private Integer totalNum;

  private String isVipOnly;

  private Long createTime;

  private Long updateTime;
}
