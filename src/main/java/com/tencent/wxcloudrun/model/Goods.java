package com.tencent.wxcloudrun.model;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class Goods implements Serializable {

  private String id;

  private String title;

  private String outOfSale;

  private Float vipPrice;

  private Float originPrice;

  private String tags;

  private String calType;

  private String thumb;

  private String thumbApp;
  public String getThumbApp() {
   return this.thumb.substring(5);
  }

  private String hotSale;

  private String desc;

  private Integer goodsNum;

  private String orderId;

  private String isDelete;

  private Long createTime;

  private Long updateTime;

  private List<String> goodsImgList;

  private List<String> goodsImgListApp;
  public List<String> getGoodsImgListApp() {
    List<String> imgList = new ArrayList<>();
    if(this.getGoodsImgs() == null) {
      return null;
    }
    for (String s: this.getGoodsImgs().split(",") ) {
      //处理图片显示问题
      if(s != null) {
        imgList.add(s.substring(5));
      }
    }
    return imgList;
  }

  private String goodsImgs; //用于页面显示

  private List<String> goodsImgDescList;

  private String[] tagArray;

  private String isSelected;  //给购物车里面使用的

  private String priceType; //订单中商品的计价类型 1： vip  0：市场价

  private String goodsClassType; //商品的类型  1：蔬菜 2：水果

  private Integer orderNum; //排序的字段

  private Integer floorBuyNum;// 起购数量

  private Integer topBuyNum; //最高限购多少件

  private String promotion; //是否促销

  private Integer stock; //促销商品数量

}
