package com.tencent.wxcloudrun.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class WxConfig implements Serializable {

  private String token;
  private String overdue;

}
