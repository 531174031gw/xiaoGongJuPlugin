package com.tencent.wxcloudrun.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
public class OrdersStatis implements Serializable {

  private String statisKey;

  private Integer statisValue;


}
