package com.tencent.wxcloudrun.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class GoodsImg implements Serializable {

  private String id;

  private String goodsId;

  private String url;

  private Integer orderNum;

  private String type;
  
  private Long createTime;

  private Long updateTime;
}
