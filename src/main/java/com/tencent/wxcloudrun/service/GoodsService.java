package com.tencent.wxcloudrun.service;

import com.tencent.wxcloudrun.model.Counter;
import com.tencent.wxcloudrun.model.Goods;
import com.tencent.wxcloudrun.model.OrdersGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface GoodsService {


  List<Goods> getGoodsPageByConditions(Map<String, Object> paramMap) ;

  Integer getGoodsTotalNumByConditions(Map<String, Object> paramMap) ;

  List<Goods> getGoodsCartByTel(String tel);

  Goods getGoodsById( String id);

  void insertGoodsCart(String tel, Goods goods);

  void deleteCartByGoodsId(String tel, String goodsId);

  void insertGoods(Goods goods);

  void updateGoods(Goods goods);

  void deleteGoods(String id);

  void updateGoodsStatus(Goods goods);

  List<OrdersGoods> getPurchaseGoodsByConditions(Map<String, Object> paramMap);

  Integer getPurchaseGoodsNumByConditions(Map<String, Object> paramMap);

  void updatePurchaseGoods(OrdersGoods ordersGoods);

  void publishGoodsPrice(String purchaseDate) throws Exception;

  void promotionGoodsAgain(String goodsId);

}
