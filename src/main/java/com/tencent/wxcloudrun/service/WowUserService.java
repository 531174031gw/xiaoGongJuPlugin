package com.tencent.wxcloudrun.service;

import com.tencent.wxcloudrun.model.Counter;
import com.tencent.wxcloudrun.model.WowUser;
import com.tencent.wxcloudrun.model.WowUserStatus;
import com.tencent.wxcloudrun.model.WxConfig;

import java.util.List;
import java.util.Optional;

public interface WowUserService {

  WowUser getUser(String tel);

  void updateUser(WowUser wowUser);

  void insertUser(WowUser WowUser);

  WowUserStatus getUserStatus(String tel);

  void updateUserStatus(WowUserStatus wowUserStatus);

  void insertUserStatus(WowUserStatus wowUserStatus);

  int countUser();

  List<WowUser> getUserTop10();

  void updateUserMsgCount(WowUser user);

  void clearToken();

  void insertToken(WxConfig wxConfig);

  WxConfig getToken();

  List<WowUser> getForAuthUsers();


}
