package com.tencent.wxcloudrun.service.impl;


import com.tencent.wxcloudrun.dao.WowUserMapper;
import com.tencent.wxcloudrun.dao.WowUserStatusMapper;
import com.tencent.wxcloudrun.model.WowUser;
import com.tencent.wxcloudrun.model.WowUserStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class FileService {

    @Autowired
    final WowUserStatusMapper userStatusMapper;

    @Value("${server.port}")
    private String port;

    @Value("${server.servlet.context-path}")
    private String contextPath;

    @Value("${pic.dir}")
    private String picDir;

    public FileService(WowUserStatusMapper userStatusMapper) {

        this.userStatusMapper = userStatusMapper;
    }


    public String saveFile(MultipartFile multipartFile, String folderName) {

        try {
            String filename = multipartFile.getOriginalFilename();
            if ( filename == null) {
                throw new Exception(); //如果获取不到用户的filename 跑出异常
            }

            String fileStr;
            if(folderName != null && !"".equals(folderName)) {
                fileStr =  folderName + "/" + filename;
            } else {
                fileStr = filename;
            }
            File file = new File(picDir + fileStr);
            //判断是否有文件夹存在，没有就创建
            if (!file.getParentFile().exists()) {
                boolean value =  file.getParentFile().mkdirs();
                if(value) {
                    log.info ("没有文件夹，创建文件夹成功！路径： " + picDir);
                } else {
                    log.info ("没有文件夹，创建文件夹失败！路径： " + picDir);
                }
            }
            //保存成功新图片后 需要删除老图片
            multipartFile.transferTo(file);
            //返回新的文件名称
            return fileStr;
        } catch (Exception e) {
            log.error("save file error,{}", e.getMessage());
            return "fail";
        }
    }

    public Boolean deleteFile(String fileName) {

        try{
            File file = new File(picDir + fileName);
            //判断是否有文件夹存在，没有就创建
            if (file.exists() ) {
                return file.delete();
            }
            return false;
        } catch (Exception e) {
            log.error("delete file error,{}", e.getMessage());
            return false;
        }
    }


    public List<String> getFiles() {
        List<String> fileUrls = new ArrayList<>();

        File file = new File(picDir);
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (files != null) {
                for (File file1 : files) {
                    fileUrls.add(getFileUrl(file1.getName()));
                }
            }
        }
        return fileUrls;
    }

    private String getFileUrl(String fileName) {
        try {
            InetAddress address = InetAddress.getLocalHost();
            String fileUrl = "http://" + address.getHostAddress() + ":" + port + contextPath + "/file/" + fileName;
            log.info("fileUrl:{}", fileUrl);
            return fileUrl;
        } catch (Exception e) {
            log.error("get host error,{}", e.getMessage());
            return "";
        }
    }

}


