package com.tencent.wxcloudrun.service.impl;

import com.tencent.wxcloudrun.dao.ToolsMapper;
import com.tencent.wxcloudrun.model.Dictionary;
import com.tencent.wxcloudrun.model.Receiver;
import com.tencent.wxcloudrun.service.CommonToolsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommonToolsServiceImpl implements CommonToolsService {


  final ToolsMapper toolsMapper;

  final Logger logger;


  public CommonToolsServiceImpl(ToolsMapper toolsMapper) {
    this.toolsMapper = toolsMapper;
    this.logger = LoggerFactory.getLogger(CommonToolsServiceImpl.class);
  }

  /**
   * 根据类型查询字典
   * @param type
   * @return
   */
  @Override
  public List<Dictionary> getDictionaryListByType(String type) {
    try{
      return toolsMapper.getDictionaryListByType(type);
    } catch (Exception e ){
      logger.error("根据类型查询字典信息失败\r\n" + e.getMessage());
      throw e;
    }
  }
}
