package com.tencent.wxcloudrun.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tencent.wxcloudrun.controller.WowUserController;
import com.tencent.wxcloudrun.dao.WowUserMapper;
import com.tencent.wxcloudrun.dao.WxConfigMapper;
import com.tencent.wxcloudrun.model.WowUser;
import com.tencent.wxcloudrun.model.WxConfig;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class WxHelpService {


    @Autowired
    final WxConfigMapper wxConfigMapper;

    @Autowired
    final WowUserMapper userMapper;


    final static Logger logger = LoggerFactory.getLogger(WowUserController.class);

    final static String appId = "wx4cc97f644f5a1352";
    final static  String appSecret = "3fd375b933b28475195c8aba1a69d1c7";
    final static  String  templateId = "nsYBSS29x3i80KkNZgh-df5vR3umqRgf5VwaYqqX6ng";     //订阅消息模板id

    public static final String SEND_INFO_URL = "https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=";

    //logincode = 033Li7Ha14AI1D0euLIa130HOU0Li7HY
    //openid = oPHcp5WtgTUyN_TCZ7CiY-8KdUW8

    public WxHelpService(WxConfigMapper wxConfigMapper, WowUserMapper userMapper) {
        this.wxConfigMapper = wxConfigMapper;
        this.userMapper = userMapper;
    }

    public static void main(String[] args) throws Exception {

        //getAccessTokenStatic();
//        String token = "56_ClhRv7NQsAbz8H3mvG2cFMtEsLJ9f6-Zbs3YJJrwt6TbU3KoRFG46Tv1dplbdF01o9bIKxYqHWfnbvLM62nrQ8N4s3xWVBWp_xzzgWoU0CpK2Bt1WGmOnMspltkkTfqFoEOt8ycgxKCvNx29PXMaACAXFU";
//        String openid = "oPHcp5WtgTUyN_TCZ7CiY-8KdUW8";
//        String snapTime = "2022-04-16 14:24:21";
//        pushMessage(openid,snapTime, token);

    }

    /**
     * 给用户发送信息
     * @param wowUser
     * @return
     */
    public void sendMsgToUser(WowUser wowUser, String snapTime, String title, String remark) {
        //获取当前token看是否过期
        WxConfig wxConfig = wxConfigMapper.getToken();
        //如果token过期 则去访问获取
        if(wxConfig == null || (new Date()).getTime() > Long.parseLong(wxConfig.getOverdue()) ) {
            try {
                wxConfig = getAccessToken();
            }catch (Exception e) {
                logger.error("获取微信小程序token失败" + e.getMessage());
            }
        }
        // 推送消息
        if (wxConfig != null ) {
            try {
                //如果用户愿意接收消息，并且剩余消息条数大于0  则发送信息
                if ("1".equals(wowUser.getIsAcceptMsg()) &&  Integer.parseInt(wowUser.getMsgCount()) > 0) {
                    //将snaptime 格式化
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String st = sdf.format(new Date(Long.parseLong(snapTime)));

                    String resultStr = pushMessage(wowUser.getOpenId(),  st, wxConfig.getToken(), title, remark);
                    if (null == resultStr) {
                        logger.error("============发送推送消息失败！");
                    } else { //消息请求成功了，需要更新用户的推送消息数
                        if ("0".equals(resultStr)) {
                            wowUser.setMsgCount(String.valueOf(Integer.parseInt(wowUser.getMsgCount()) - 1));
                            logger.info("============ 给用户推送消息成功！");
                        } else if ("43101".equals(resultStr)) { //用户拒绝消息推送，也就是没有消息推送的订阅了，所以数据库中的次数清零
                            wowUser.setMsgCount(String.valueOf("0"));
                            logger.info("============ 给用户推送消息失败，原因： 用户没有订阅消息！（腾讯系统返回）");
                        }
                        //保存用户的信息
                        userMapper.updateUserMsgCount(wowUser);
                    }
                } else {
                    logger.info("============ 给用户推送消息失败，原因： 用户没有订阅！（内部系统判断）");
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        } else {

            logger.info("一直没有获取到token，请检查！");
        }
    }


    public String getUserOpenId(String loginCode) throws Exception {
        String url = "https://api.weixin.qq.com/sns/jscode2session?appid=" + appId + "&secret=" + appSecret + "&grant_type=authorization_code&js_code=" + loginCode;
        //发送请求
        String result = sendGet(url);
        JSONObject obj = JSON.parseObject(result);
        String openid = obj.getString("openid");

        if (StringUtils.isEmpty(openid)) {
            logger.error("获取openid失败: {}" , obj.getString("errmsg"));
            throw new Exception("获取openid失败");
        } else {
            logger.info("wx openid : {}",openid);
            return openid;
        }
    }

    public static String pushMessage(String openId, String snapTime, String token, String title, String remark) throws Exception {

        String url =  SEND_INFO_URL  + token;
        String resultStr = null;
        Map<String, Object> phrase1 = new HashMap<>();
        phrase1.put("value", title);
        Map<String, Object> thing2 = new HashMap<>();
        thing2.put("value", remark);
        Map<String, Object> time8 = new HashMap<>();
        time8.put("value", snapTime);

        Map<String, Object> dataParameters = new HashMap<>();
        dataParameters.put("phrase1", phrase1);
        dataParameters.put("thing2", thing2);
        dataParameters.put("time8", time8);

        Map<String, Object> urlParameters = new HashMap<>();
        urlParameters.put("touser", openId);
        urlParameters.put("template_id", templateId);
        urlParameters.put("data", dataParameters);

        logger.info("消息推送的请求参数：" + JSONObject.toJSONString(urlParameters));
        String result = sendPost(url, JSONObject.toJSONString(urlParameters));
        logger.info("发送信息，返回参数：" + result);

        //异常信息格式
        // {"errcode":43101,"errmsg":"user refuse to accept the msg rid: 625e2f88-259da820-32937b87"}
        //正常数据格式
        //{"errcode":0,"errmsg":"ok","msgid":2361361731479961600}
        JSONObject obj = JSON.parseObject(result);
        resultStr = obj.getString("errcode");

        return resultStr;
    }


    public WxConfig getAccessToken() throws Exception {

        String WX_ACCESS_TOKEN = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+ appId +"&secret=" + appSecret;
        //发送请求
        String result = sendGet(WX_ACCESS_TOKEN);
        JSONObject obj = JSON.parseObject(result);
        String accessToken = obj.getString("access_token");
        String overdue = obj.getString("expires_in");
        WxConfig wxConfig = new WxConfig();
        wxConfig.setToken(accessToken);
        //把当前时间加上 得到最后的过期时间
        Long overdueTime = (new Date()).getTime() + Long.parseLong(overdue) * 1000 - 600 * 1000;  //减数字是为了提前获取token
        wxConfig.setOverdue(String.valueOf(overdueTime));
        if (StringUtils.isEmpty(accessToken)) {
            logger.error("获取access token失败: {}" , obj.getString("errmsg"));
            throw new Exception("获取access token失败");
        } else {
            logger.info("wx access_token : {}",accessToken);
            //保存token
            wxConfigMapper.clearToken();
            wxConfigMapper.insertToken(wxConfig);
            return wxConfig;
        }
    }

    // HTTP GET请求
    private static String sendGet(String url) throws Exception {

        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(url);

        //添加请求头
        request.addHeader("User-Agent", "Mozilla/5.0");
        HttpResponse response = client.execute(request);

        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " +
                response.getStatusLine().getStatusCode());

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        StringBuilder result = new StringBuilder();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        System.out.println(result.toString());

        return result.toString();

    }

    // HTTP POST请求
    private static String sendPost(String url, String urlParameters) throws Exception {

        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(url);

        //添加请求头
        post.setHeader("User-Agent", "Mozilla/5.0");
        post.setEntity(new StringEntity(urlParameters, "UTF-8"));

        HttpResponse response = client.execute(post);
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + post.getEntity());
        System.out.println("Response Code : " +
                response.getStatusLine().getStatusCode());

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        StringBuilder result = new StringBuilder();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        System.out.println(result.toString());
        return result.toString();

    }





}
