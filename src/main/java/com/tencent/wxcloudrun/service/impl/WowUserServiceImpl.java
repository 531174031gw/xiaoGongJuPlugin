package com.tencent.wxcloudrun.service.impl;

import com.tencent.wxcloudrun.dao.WowUserMapper;
import com.tencent.wxcloudrun.dao.WowUserStatusMapper;
import com.tencent.wxcloudrun.dao.WxConfigMapper;
import com.tencent.wxcloudrun.model.WowUser;
import com.tencent.wxcloudrun.model.WowUserStatus;
import com.tencent.wxcloudrun.model.WxConfig;
import com.tencent.wxcloudrun.service.WowUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WowUserServiceImpl implements WowUserService {

  @Autowired
  final WowUserStatusMapper userStatusMapper;
  @Autowired
  final WowUserMapper userMapper;

  @Autowired
  final WxConfigMapper wxConfigMapper;

  public WowUserServiceImpl(WowUserStatusMapper userStatusMapper, WowUserMapper userMapper, WxConfigMapper wxConfigMapper) {
    this.userStatusMapper = userStatusMapper;
    this.userMapper = userMapper;
    this.wxConfigMapper = wxConfigMapper;
  }

  public void WowUserServiceImpl(){

  }

  /**
   * 获取用户信息
   * @param tel
   * @return
   */
  @Override
  public WowUser getUser(String tel) {
    if (tel == null || tel.isEmpty()) {
      return null;
    }

    return userMapper.getUser(tel);
  }

  @Override
  public void updateUser(WowUser wowUser) {
    if (wowUser == null) {
      return;
    }
    userMapper.updateUser(wowUser);
  }

  @Override
  public void insertUser(WowUser wowUser) {
    if (wowUser == null) {
      return;
    }
    userMapper.insertUser(wowUser);
  }


  @Override
  public WowUserStatus getUserStatus(String tel) {
    if (tel == null || tel.isEmpty()) {
      return null;
    }
    return userStatusMapper.getUserStatus(tel);
  }

  @Override
  public void updateUserStatus(WowUserStatus wowUserStatus) {
    if (wowUserStatus == null) {
      return;
    }
    userStatusMapper.updateUserStatus(wowUserStatus);
  }

  @Override
  public void insertUserStatus(WowUserStatus wowUserStatus) {
    if (wowUserStatus == null) {
      return;
    }
    userStatusMapper.insertUserStatus(wowUserStatus);
  }

  @Override
  public int countUser() {
    return userMapper.countUser();
  }

  @Override
  public List<WowUser> getUserTop10() {
    return userMapper.getUserTop10();
  }

  @Override
  public void updateUserMsgCount(WowUser user) {
    userMapper.updateUserMsgCount(user);
  }

  @Override
  public void clearToken() {
      wxConfigMapper.clearToken();
  }

  @Override
  public void insertToken(WxConfig wxConfig) {
      wxConfigMapper.insertToken(wxConfig);
  }

  @Override
  public WxConfig getToken() {
    return wxConfigMapper.getToken();
  }

  @Override
  public List<WowUser> getForAuthUsers() {
    return userMapper.getForAuthUsers();
  }

}
