package com.tencent.wxcloudrun.service.impl;

import com.tencent.wxcloudrun.dao.GoodsMapper;
import com.tencent.wxcloudrun.model.Goods;
import com.tencent.wxcloudrun.model.GoodsImg;
import com.tencent.wxcloudrun.model.OrdersGoods;
import com.tencent.wxcloudrun.service.GoodsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class GoodsServiceImpl implements GoodsService {


  final GoodsMapper goodsMapper;

  final Logger logger;


  public GoodsServiceImpl( GoodsMapper goodsMapper) {
    this.goodsMapper = goodsMapper;
    this.logger = LoggerFactory.getLogger(GoodsServiceImpl.class);
  }

  /**
   *  通过条件获取商品的列表
   * @return
   */
  @Override
  public List<Goods> getGoodsPageByConditions(Map<String, Object> paramMap) {

    try{

      List<Goods> goodsList = goodsMapper.getGoodsPageByConditions(paramMap);
      return goodsList;
    } catch (Exception e) {
      logger.error("根据商品类型查询商品列表失败\r\n" + e.getMessage());
      throw e;
    }
  }

  @Override
  public Integer getGoodsTotalNumByConditions(Map<String, Object> paramMap) {
    try{

      return goodsMapper.getGoodsTotalNumByConditions(paramMap);
    } catch (Exception e) {
      logger.error("根据商品类型查询商品总数失败\r\n" + e.getMessage());
      throw e;
    }
  }

  /**
   * 根据手机号 获取购物车信息
   * @param tel
   * @return
   */
  @Override
  public List<Goods> getGoodsCartByTel(String tel) {

    try{
      return goodsMapper.getGoodsCartByTel(tel);
    } catch (Exception e) {
      logger.error("根据手机号获取购物车列表信息失败\r\n" + e.getMessage());
      throw e;
    }
  }

  /**
   * 根据id查询商品信息
   * @param id
   * @return
   */
  @Override
  public Goods getGoodsById(String id) {

    try{
      Goods goods = goodsMapper.getGoodsById(id);
      List<GoodsImg> goodsImgList = goodsMapper.getGoodsImgByIds(goods.getId());
      StringBuilder goodsImgs = new StringBuilder();
      for (GoodsImg gi: goodsImgList ) {
          goodsImgs.append(gi.getUrl()).append(",");
      }
      if(!"".equals(goodsImgs.toString())) {
        goods.setGoodsImgs(goodsImgs.substring(0, goodsImgs.length()-1));
      }
      return goods;
    } catch (Exception e) {
      logger.error("根据id获取商品信息失败\r\n" + e.getMessage());
      throw e;
    }
  }

  /**
   *
   * 插入用户购物车信息商品信息
   * @param tel
   * @param goods
   */
  @Override
  public void insertGoodsCart(String tel, Goods goods) {

    try{
      //先查询是否存在
      Goods goodsCart = goodsMapper.getGoodsCartByTelId(tel, goods.getId());
      if (goodsCart != null) {
        logger.info("已有购物车商品数据，在此上添加数量");
        if(goods.getGoodsNum() == 1) { //如果修改的商品数量大于一，则表示这是一次直接赋值修改，否则就是在原有的基础上加一
          goods.setGoodsNum(goods.getGoodsNum() + goodsCart.getGoodsNum());
        }
        goodsMapper.updateGoodsCart(tel, goods);
      } else {
        logger.info("没有购物车商品数据，添加数据");
        goodsMapper.insertGoodsCart(tel, goods);
      }
    }catch (Exception e){
      logger.error("插入购车商品信息失败！！\r\n" + e.getMessage());
      throw e;
    }
  }

  /**
   * 删除购物车商品信息
   * @param tel
   * @param goodsId
   */
  @Override
  public void deleteCartByGoodsId(String tel, String goodsId) {

      try{
        goodsMapper.deleteCartByGoodsId(tel, goodsId);
      }catch (Exception e){
        logger.error("删除购车商品信息失败！！\r\n" + e.getMessage());
        throw e;
      }
  }

  /**
   * 插入商品数据
   * @param goods
   */
  @Transactional(rollbackFor = Exception.class)
  @Override
  public void insertGoods(Goods goods) {

    try{
      goodsMapper.insertGoods(goods);
      //插入商品的图片
      if(goods.getGoodsImgList() != null && goods.getGoodsImgList().size() > 0) {
        List<GoodsImg> goodsImgList = new ArrayList<>();
        for(int i=0;  i < goods.getGoodsImgList().size(); i++) {
          GoodsImg gi = new GoodsImg();
          gi.setGoodsId(goods.getId());
          gi.setUrl(goods.getGoodsImgList().get(i));
          gi.setOrderNum(i);
          gi.setCreateTime(goods.getCreateTime());
          gi.setUpdateTime(goods.getUpdateTime());
          gi.setType("1"); //默认都用类型1
          goodsImgList.add(gi);
        }
        goodsMapper.insertGoodsImg(goodsImgList);
      }
    }catch (Exception e){
      logger.error("插入商品信息失败！！\r\n" + e.getMessage());
      throw e;
    }
  }

  /**
   * 更新商品数据
   * @param goods
   */
  @Transactional(rollbackFor = Exception.class)
  @Override
  public void updateGoods(Goods goods) {

    try{
      Goods g = goodsMapper.getGoodsById(goods.getId());
      if(g == null) {
        logger.error("更新的商品数据不存在！");
        throw new Exception();
      }
      goods.setId(g.getId());
      goodsMapper.updateGoods(goods);
      //更新图片
      //先删除所有的图片
      goodsMapper.deleteGoodsImgByGoodsId(goods.getId());
      //重新插入图片
      if(goods.getGoodsImgList() != null && goods.getGoodsImgList().size() > 0) {
        List<GoodsImg> goodsImgList = new ArrayList<>();
        for(int i=0;  i < goods.getGoodsImgList().size(); i++) {
          GoodsImg gi = new GoodsImg();
          gi.setGoodsId(goods.getId());
          gi.setUrl(goods.getGoodsImgList().get(i));
          gi.setOrderNum(i);
          gi.setCreateTime(goods.getCreateTime());
          gi.setUpdateTime(goods.getUpdateTime());
          gi.setType("1"); //默认都用类型1
          goodsImgList.add(gi);
        }
        goodsMapper.insertGoodsImg(goodsImgList);
      }
    }catch (Exception e){
      logger.error("更新商品信息失败！！\r\n" + e.getMessage());
    }
  }

  /**
   * 删除商品
   * @param id
   */
  @Override
  public void deleteGoods(String id) {

    try{
      goodsMapper.deleteGoodsById(id);
    }catch (Exception e){
      logger.error("更新商品信息失败！！\r\n" + e.getMessage());
      throw e;
    }
  }

  /**
   * 商品上下架
   */
  @Override
  public void updateGoodsStatus(Goods goods) {

    try{
      goodsMapper.updateGoodsStatus(goods);
    }catch (Exception e){
      logger.error("商品上下架失败！！\r\n" + e.getMessage());
      throw e;
    }
  }

  /**
   * 获取订单商品统计
   * @param paramMap
   * @return
   */
  @Override
  public List<OrdersGoods> getPurchaseGoodsByConditions(Map<String, Object> paramMap) {

    try{
        return goodsMapper.getPurchaseGoodsByConditions(paramMap);
      }catch (Exception e){
        logger.error("获取订单商品统计失败！！\r\n" + e.getMessage());
        throw e;
      }
  }

  /**
   * 统计订单列表数
   * @param paramMap
   * @return
   */
  @Override
  public Integer getPurchaseGoodsNumByConditions(Map<String, Object> paramMap) {
      try{
        return goodsMapper.getPurchaseGoodsNumByConditions(paramMap);
      }catch (Exception e){
        logger.error("统计订单列表数失败！！\r\n" + e.getMessage());
        throw e;
      }
  }

  /**
   * 更新订单商品采购数据
   * @param ordersGoods
   */
  @Override
  public void updatePurchaseGoods(OrdersGoods ordersGoods) {

    try{
      goodsMapper.updatePurchaseGoodsPrice(ordersGoods);
      goodsMapper.updatePurchaseGoods(ordersGoods);
    }catch (Exception e){
      logger.error("更新订单商品采购数据失败！！\r\n" + e.getMessage());
      throw e;
    }
  }

  /**
   * 发布商品的价格
   * @param purchaseDate
   */
  @Transactional(rollbackFor = Exception.class)
  @Override
  public void publishGoodsPrice(String purchaseDate) throws Exception {

    try{
      Map<String, Object> paramMap = new HashMap<>();
      paramMap.put("purchaseDate", purchaseDate);
      paramMap.put("currentPage", 0);
      paramMap.put("pageSize", 10000);
      List<OrdersGoods> ordersGoodsList = goodsMapper.getPurchaseGoodsByConditions(paramMap);
      if(ordersGoodsList == null || ordersGoodsList.isEmpty()) {
        throw  new Exception("采购日期： " + purchaseDate + ", 没有查询到采购商品信息！");
      }
      for(OrdersGoods og: ordersGoodsList){
        //如果当天的商品有没有更新 上货价的 则不能发布价格
        if(og.getVipPrice() == null || og.getVipPrice() == 0) {
          throw  new Exception("采购日期： " + purchaseDate + ", 还有商品未更新上货价，请核对后再发布价格！");
        }
      }

      //循环更新价格
      for(OrdersGoods og: ordersGoodsList){
        goodsMapper.publishGoodsPrice(og);
      }
    }catch (Exception e){
      logger.error("更新订单商品采购数据失败！！\r\n" + e.getMessage());
      throw e;
    }

  }

  /**
   * 删除和制定促销商品绑定的用户，再次促销
   * @param goodsId
   */
  @Override
  public void promotionGoodsAgain(String goodsId) {
    try{
      goodsMapper.deletePromotionGoodsById(goodsId);
    }catch (Exception e){
      logger.error("删除和制定促销商品绑定的用户失败！！\r\n" + e.getMessage());
      throw e;
    }


  }


  /**
   * todo 定时任务 每天凌晨把昨天的订单商品统计到 采购表中
   */
  public void insertPurchaseGoods(){

  }









}
