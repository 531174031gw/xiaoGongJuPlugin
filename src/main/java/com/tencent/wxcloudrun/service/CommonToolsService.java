package com.tencent.wxcloudrun.service;

import com.tencent.wxcloudrun.model.Dictionary;
import com.tencent.wxcloudrun.model.Receiver;

import java.util.List;

public interface CommonToolsService {


  List<Dictionary> getDictionaryListByType (String type);


}
