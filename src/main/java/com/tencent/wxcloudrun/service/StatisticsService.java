package com.tencent.wxcloudrun.service;

import com.tencent.wxcloudrun.dto.StatisticsRequest;
import com.tencent.wxcloudrun.model.Receiver;
import com.tencent.wxcloudrun.model.StatisticsBasic;
import com.tencent.wxcloudrun.model.StatisticsModel;

import java.text.ParseException;
import java.util.List;

public interface StatisticsService {


  StatisticsBasic getStatisticsBasic(StatisticsRequest request) throws ParseException;

  List<StatisticsModel> getOrderStatistics(StatisticsRequest request);

  List<StatisticsModel> getUserStatistics(StatisticsRequest request);

  List<StatisticsModel> getGoodsClassSaleStatistics(StatisticsRequest request);

  List<StatisticsModel> getGoodsSaleStatistics(StatisticsRequest request);




}
