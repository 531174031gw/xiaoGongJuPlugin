package com.tencent.wxcloudrun.service;

import com.tencent.wxcloudrun.model.Goods;
import com.tencent.wxcloudrun.model.Operation;
import com.tencent.wxcloudrun.model.Orders;

import java.util.List;
import java.util.Map;

public interface OrdersService {


  List<Orders> getOrdersPageByTel(String status, String tel, int currentPage, int pageSize) throws Exception;

  void insertOrders( Orders orders );

  void  updateOrdersStatus(Orders orders );

  List<Orders> getOrderByConditions(Map<String, Object> paramMap);

  Integer getOrderCountByConditions(Map<String, Object> paramMap);

  Orders getOrderById(String id) throws Exception;

  void insertOrderOperationRemark(Operation operation);

  void updateOrdersGoodsNum(Orders orders, Operation operation);

  void updateOrderStatusToDeliver(Orders orders, String operator);

}
