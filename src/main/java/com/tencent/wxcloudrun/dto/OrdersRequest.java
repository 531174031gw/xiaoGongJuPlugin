package com.tencent.wxcloudrun.dto;

import com.tencent.wxcloudrun.model.Goods;
import com.tencent.wxcloudrun.model.OrdersGoods;
import lombok.Data;

import java.util.List;

@Data
public class OrdersRequest {

  private String id;

  private String status;

  private String tel;

  private String keyWord;

  private Integer currentPage;

  private Integer pageSize;

  private String receiveTime;

  private String receiverId;

  private String receiverName;

  private String receiverTel;

  private String province;

  private String city;

  private String district;

  private String addrDetail;

  private String areaId;

  private String dsId;

  private String estateId;

  private Long queryTime;

  private String remark;

  private String operator; //操作人

  private String operation; //操作备注

  private Long createTime;

  private Long updateTime;

  private List<OrdersGoods> goodsList;

//  private List<OrdersGoods> ordersGoodsList;

}
