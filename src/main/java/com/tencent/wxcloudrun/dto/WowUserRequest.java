package com.tencent.wxcloudrun.dto;

import lombok.Data;

import java.util.List;

@Data
public class WowUserRequest {

  private String tel;
  private String avator;
  private String nickName;
  private String snapPicture;
  private String snapTime;
  private String isOnline;

  private String vipOverdue;
  private String chargeStatus;

  private String userCount;
  private List<String> avatorList;

  private String loginCode;

  private boolean isSuccess;

}
