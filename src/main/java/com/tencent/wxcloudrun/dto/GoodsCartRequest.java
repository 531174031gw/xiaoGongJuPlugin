package com.tencent.wxcloudrun.dto;

import lombok.Data;

@Data
public class GoodsCartRequest {

  private String goodsId;

  private String tel;

  private int goodsNum;

}
