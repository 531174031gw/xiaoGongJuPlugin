package com.tencent.wxcloudrun.dto;

import lombok.Data;

@Data
public class UserRequest {

  private String tel;

  private String password;

  private String openId;

  private String avatar;

  private String nickname;

  private String verifyCode; // 短信验证码

  private Long vipStartTime;

  private Long vipEndTime;

  private String loginCode;

  private String keyWord;

  private int currentPage;

  private int pageSize;

  private String[] roleIds; // 用户分配的角色id

  private String actived;

  private String remark;

  private String area;

  private String distributionStation;

}
