package com.tencent.wxcloudrun.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Calendar;
import java.util.Date;

/**
 * @version 1.0
 * @description: 生成token
 * @date 2022/4/22 8:51
 */
public class TokenUtils {

    //JWT存储的请求头
    public final static String TOKEN_HEADER = "Authorization";
    //JWT加解密使用的密钥
    public final static String SECRET = "mall-admin-secret";
    //过期时间  单位小时
    public final static Integer EXPIRATION = 2;
    //#JWT负载中拿到开头
    public final static String TOKEN_HEAD = "Bearer ";


    /**
     * @description:  生成token
     * @author 车矣捕
     * @date: 2022/4/22
     */
    public static String getToken(String userId,String sign){
        return JWT.create().withAudience(userId) // 将 user id 保存到 token 里面 作为载荷
                .withExpiresAt(caculateExpTime(new Date(),EXPIRATION)) //两小时后token过期
                .sign(Algorithm.HMAC256(sign)); // 以 sign 作为 token 的密钥

    }

    //计算过期时间
    private static Date caculateExpTime(Date date, int hours) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR, hours);
        return calendar.getTime();
    }


    private static final long EXPIRE_TIME= 15*60*1000;
    private static final String TOKEN_SECRET="token123";  //密钥盐

    /**
     * 签名生成
     * @return
     */
    public static String sign(String name){

        String token = null;
        try {
            Date expiresAt = new Date(System.currentTimeMillis() + EXPIRE_TIME);
            token = JWT.create()
                    .withIssuer("auth0").withClaim("id","id")
                    .withClaim("username", name)
                    .withExpiresAt(expiresAt)
                    // 使用了HMAC256加密算法。
                    .sign(Algorithm.HMAC256(TOKEN_SECRET));
        } catch (Exception e){
            e.printStackTrace();
        }
        return token;

    }
    /**
     * 签名验证
     * @param token
     * @return
     */
    public static boolean verify(String token){

        try {
            JWTVerifier verifier = JWT.require(Algorithm.HMAC256(TOKEN_SECRET)).withIssuer("auth0").build();
            DecodedJWT jwt = verifier.verify(token);
//            System.out.println("认证通过：");
//            System.out.println("issuer: " + jwt.getIssuer());
//            System.out.println("username: " + jwt.getClaim("username").asString());
//            System.out.println("id"+jwt.getClaim("id").asString());
//            System.out.println("过期时间：      " + jwt.getExpiresAt());
            return true;
        } catch (Exception e){
            return false;
        }
    }

    /**
     * 签名验证
     * @param token
     * @return
     */
    public static String getUsername(String token){

        try {
            JWTVerifier verifier = JWT.require(Algorithm.HMAC256(TOKEN_SECRET)).withIssuer("auth0").build();
            DecodedJWT jwt = verifier.verify(token);
//            String username = jwt.getClaim("username").asString();
//            System.out.println("登录用户的username: " + username);

            return jwt.getClaim("username").asString();
        } catch (Exception e){
            System.out.println("获取登录用户的username失败！" + e.getMessage());
            return null;
        }
    }


}


