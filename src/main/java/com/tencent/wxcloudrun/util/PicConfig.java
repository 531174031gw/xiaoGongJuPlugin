package com.tencent.wxcloudrun.util;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Slf4j
@Configuration
public class PicConfig implements WebMvcConfigurer {

    @Value("${pic.dir}")
    private String picDir;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        log.info("获取到图片的映射地址" + picDir);

        registry.addResourceHandler("/file/**")
                .addResourceLocations("file:" + picDir);
    }
}


