package com.tencent.wxcloudrun.dao;

import com.tencent.wxcloudrun.model.Counter;
import com.tencent.wxcloudrun.model.WowUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface WowUserMapper {

  WowUser getUser(@Param("tel") String tel);

  void updateUser(WowUser user);

  void insertUser(WowUser user);

  int countUser();

  List<WowUser> getUserTop10();

  void updateUserMsgCount(WowUser user);

  List<WowUser> getForAuthUsers();
}
