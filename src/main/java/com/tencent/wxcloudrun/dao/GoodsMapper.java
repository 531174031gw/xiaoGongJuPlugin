package com.tencent.wxcloudrun.dao;

import com.tencent.wxcloudrun.model.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface GoodsMapper {

  List<Goods> getGoodsPageByConditions(Map<String, Object> paramMap);

  Integer getGoodsTotalNumByConditions(Map<String, Object> paramMap);

  List<Goods> getGoodsCartByTel(@Param("tel") String tel);

  Goods getGoodsById(@Param("id") String id);

  List<GoodsImg> getGoodsImgByIds(@Param("goodsId") String goodsId);

  List<OrdersGoods> getGoodsByOrdersIds(List<String> orderIds);

  void insertGoodsCart(@Param("tel") String tel, @Param("goods") Goods goods);

  void deleteCartByGoodsId(@Param("tel") String tel, @Param("goodsId") String goodsId);

  Goods getGoodsCartByTelId(@Param("tel") String tel, @Param("goodsId") String goodsId);

  void updateGoodsCart(@Param("tel") String tel, @Param("goods") Goods goods);

  void insertGoods(Goods goods);

  void updateGoods( Goods goods);

  void deleteGoodsById(@Param("id") String id );

  void insertGoodsImg(@Param("list") List<GoodsImg> goodsImgList);

  void deleteGoodsImgByGoodsId(@Param("goodsId") String goodsId );

  void updateGoodsStatus(Goods goods);

  List<OrdersGoods> getPurchaseGoodsByConditions(Map<String, Object> paramMap);

  Integer getPurchaseGoodsNumByConditions(Map<String, Object> paramMap);

  void insertPurchaseGoodsPrice(List<OrdersGoods> ordersGoodsList);

  void insertPurchaseGoods(List<OrdersGoods> ordersGoodsList);

  void updatePurchaseGoodsPrice(OrdersGoods og);

  void updatePurchaseGoods(OrdersGoods og);

  void publishGoodsPrice(OrdersGoods og);

  List<UserPromotionGoods> getUserPromotionGoodsByTel(@Param("tel") String tel);

  void deletePromotionGoodsById(@Param("goodsId") String goodsId);

  void insertUserPromotionGoods(UserPromotionGoods upg);

}
