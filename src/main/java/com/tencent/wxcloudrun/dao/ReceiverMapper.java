package com.tencent.wxcloudrun.dao;

import com.tencent.wxcloudrun.model.CommonArea;
import com.tencent.wxcloudrun.model.Goods;
import com.tencent.wxcloudrun.model.GoodsImg;
import com.tencent.wxcloudrun.model.Receiver;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ReceiverMapper {

  List<Receiver> getReceiverByTel(@Param("tel") String tel);

  void insertReceiver( Receiver receiver );

  void  updateReceiver(Receiver receiver );

  List<CommonArea> getAreaByConditions(Map<String, Object> paramMap);

  List<CommonArea> getEstateListByArea(Map<String, Object> paramMap);

  
}
