package com.tencent.wxcloudrun.dao;

import com.tencent.wxcloudrun.model.CommonArea;
import com.tencent.wxcloudrun.model.Operation;
import com.tencent.wxcloudrun.model.Receiver;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface OperationMapper {

  List<Operation> getOperationByConditions(Map<String, Object> paramMap);

  void insertOperation( Operation operation );

}
