package com.tencent.wxcloudrun.dao;

import com.tencent.wxcloudrun.model.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface OrdersMapper {

  List<Orders> getOrdersPageByTel(Map<String, Object> paramMap);

  void insertOrders( Orders orders );

  void insertOrdersGoods( List<Goods> goodsList );

  void  updateOrdersStatus(Orders orders );

  List<OrdersStatis> countOrdersByTel(@Param("tel") String tel);

  List<OrdersStatis> countTotalPriceByTel(@Param("tel") String tel, @Param("vipStartTime") Long vipStartTime,
                                          @Param("vipEndTime") Long vipEndTime );

  List<Orders> getOrderByConditions(Map<String, Object> paramMap);

  Integer getOrderCountByConditions(Map<String, Object> paramMap);

  Orders getOrderById(@Param("id") String id);

  void updateOrdersGoods(OrdersGoods ordersGoods);
  
}
